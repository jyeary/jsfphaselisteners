/*
 * Copyright 2010 Blue Lotus Software, LLC.
 * Copyright 2007-2010 John Yeary <jyeary@bluelotussoftware.com>.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
/*
 * PhasePrinterListener.java
 *
 * Created on February 3, 2007, 11:18 PM
 *
 * $Id$
 */
package com.bluelotussoftware.jsf.phaselisteners;

import java.util.logging.Logger;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

/**
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.1
 */
public class PhasePrinterListener implements PhaseListener {

    private static final Logger logger = Logger.getLogger("global");
    private static final long serialVersionUID = 3131268230269004403L;

    /**
     * Creates a new instance of PhasePrinterListener
     */
    public PhasePrinterListener() {
    }

    @Override
    public void afterPhase(PhaseEvent event) {
        logger.info("After - " + event.getPhaseId().toString());

        if (event.getPhaseId() == PhaseId.RENDER_RESPONSE) {
            logger.info("Done with request!\n");
        }
    }

    @Override
    public void beforePhase(PhaseEvent event) {

        if (event.getPhaseId() == PhaseId.RESTORE_VIEW) {
            logger.info("Processing new request!");
        }
        logger.info("Before - " + event.getPhaseId().toString());
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.ANY_PHASE;
    }
}
